import service from '@/util/httpApi'

export function makeCaptcha() {
    return service({
        url: '/captcha/makeCaptcha',
        method: 'get'
    })
}

export function getCaptcha(data) {
    return service({
        url: '/captcha/getCaptcha',
        method: 'post',
        data: data
    })
}
