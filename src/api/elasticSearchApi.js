import service from '@/util/httpApi'

export function search(data) {
    return service({
        url: '/elasticSearch/searchStr',
        method: 'post',
        data: data
    })
}