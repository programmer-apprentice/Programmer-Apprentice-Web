import service from '@/util/httpApi'

export function hotBlog() {
  return service({
    url: '/apprentice/blog/hotBlog',
    method: 'get'
  })
}
export function newBlog(data) {
  return service({
    url: '/apprentice/blog/newBlog',
    method: 'post',
    data: data
  })
}




