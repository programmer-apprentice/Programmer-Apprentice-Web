import service from '@/util/httpApi'

export function getNotifyUserSetting() {
    return service({
        url: '/apprentice/userSetting/getNotifyUserSetting',
        method: 'post'
    })
}
export function notifyUserSetting(data) {
    return service({
        url: '/apprentice/userSetting/notifyUserSetting',
        method: 'post',
        data: data
    })
}
