import service from '@/util/httpApi'

export function notifyPage(data) {
    return service({
        url: '/apprentice/notify/page',
        method: 'post',
        data: data
    })
}
export function count() {
    return service({
        url: '/apprentice/notify/count',
        method: 'get'
    })
}

export function updateStatus() {
    return service({
        url: '/apprentice/notify/updateStatus',
        method: 'get'
    })
}