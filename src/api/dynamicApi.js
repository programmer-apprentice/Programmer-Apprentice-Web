import service from '@/util/httpApi'

export function newDynamic(data) {
    return service({
        url: '/apprentice/dynamic/newDynamic',
        method: 'post',
        data: data
    })
}

export function dynamicInsert(data) {
    return service({
        url: '/apprentice/dynamic/insert',
        method: 'post',
        data: data
    })
}
export function getById(id) {
    return service({
        url: '/apprentice/dynamic/getById?id='+ id,
        method: 'get'
    })
}
