import service from '@/util/httpApi'

export function sendMasterApprenticeAsk(date) {
    return service({
        url: '/apprentice/apprent/sendMasterApprenticeAsk',
        method: 'post',
        data: date
    })
}

export function askList() {
    return service({
        url: '/apprentice/apprent/askList',
        method: 'get'
    })
}

export function listApprentice() {
    return service({
        url: '/apprentice/apprent/listApprentice',
        method: 'get'
    })
}

export function listMaster() {
    return service({
        url: '/apprentice/apprent/listMaster',
        method: 'get'
    })
}
export function findById(id) {
    return service({
        url: '/apprentice/apprent/findById?id='+id,
        method: 'get'
    })
}
export function updateStatus(date) {
    return service({
        url: '/apprentice/apprent/updateStatus',
        method: 'post',
        data: date
    })
}


