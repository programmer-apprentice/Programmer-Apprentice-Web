import service from '@/util/httpApi'

export function loginvalidate(data) {
  return service({
    url: '/loginvalidate',
    method: 'post',
    data: data
  })
}

export function getUserInfoForId(userId) {
  return service({
    url: '/user/getUserInfoForId?userId='+userId,
    method: 'get'
  })
}
export function getUserInfo() {
  return service({
    url: '/user/getUserInfo',
    method: 'get'
  })
}

export function logout() {
  return service({
    url: '/logoutSystem',
    method: 'get'
  })
}
// 发送邮箱验证
export function sendEmailRegisterCode(email) {
  return service({
    url: '/user/sendEmailRegisterCode?email=' + email,
    method: 'get'
  })
}
// 注册
export function userRegiste(data) {
  return service({
    url: '/user/register',
    method: 'post',
    data
  })
}
// 注册
export function updateUserInfo(data) {
  return service({
    url: '/apprentice/userSetting/updateUserInfo',
    method: 'post',
    data
  })
}

