import service from '@/util/httpApi'

export function getBlog(id) {
  return service({
    url: '/apprentice/blog/getById?id='+id,
    method: 'get'
  })
}

export function findByEsId(id) {
  return service({
    url: '/apprentice/blog/findByEsId?elasticSearchId='+id,
    method: 'get'
  })
}

export function insertBlog(data) {
  return service({
    url: '/apprentice/blog/insert',
    method: 'post',
    data: data
  })
}


