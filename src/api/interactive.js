import service from '@/util/httpApi'


export function insertGood(data) {
    return service({
        url: '/apprentice/good/insert',
        method: 'post',
        data: data,
        contentType: 'application/json;charset=UTF-8'
    })
}

export function insertComment(data) {
    return service({
        url: '/apprentice/comment/insert',
        method: 'post',
        data: data
    })
}

export function getByBlogId(params, data) {
    return service({
        url: '/apprentice/comment/getByBlogId?blogId=' + params,
        method: 'post',
        data
    })
}

export function pageComment(data) {
    return service({
        url: '/apprentice/comment/page',
        method: 'post',
        data: data
    })
}


export function insertCollection(data) {
    return service({
        url: '/apprentice/collection/insert',
        method: 'post',
        data: data
    })
}
