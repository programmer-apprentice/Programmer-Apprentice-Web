import service from '@/util/httpApi'

export function simpleCode(data) {
    return service({
        headers: {
            'content-type': 'application/json'
        },
        url: '/plugin/jsonCode/simpleCode',
        method: 'post',
        data: data
    })
}