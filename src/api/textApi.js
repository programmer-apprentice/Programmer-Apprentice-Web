import service from '@/util/httpApi'

export function listText(data) {
    return service({
        url: '/text/listText',
        method: 'post',
        data: data
    })
}