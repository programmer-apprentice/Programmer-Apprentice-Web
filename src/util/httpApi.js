import axios from 'axios'
import { getToken } from './auth'
import Router from "vue-router";


//创建axios实例
const service = axios.create({
    baseURL: '/api',
    timeout: 10000
})
// request interceptor
service.interceptors.request.use(
    config => {
        let token = getToken();
        if (typeof (token) != "undefined") {
            config.headers.Authorization = token;
        }
        // do something before request is sent
        return config
    },
    error => {
        // do something with request error
        console.log(error) // for debug
        return Promise.reject(error)
    }
);

// response interceptor
service.interceptors.response.use(
    response => {

        const res = response.data
        if (res.code === "0401"){
            new Router().push("/");
            location.reload();
        }
        return res;
    },
    error => {
        console.log('err' + error) // for debug
    }
)
export default service
