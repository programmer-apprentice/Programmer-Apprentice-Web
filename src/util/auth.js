import Cookies from 'js-cookie'

const TokenKey = 'Authorization'

export function getToken() {
  return Cookies.get(TokenKey)
}

export function getCustomizeToken(key) {
  return Cookies.get(key)
}

export function setToken(token) {
  return Cookies.set(TokenKey, token)
}

export function setCustomizeToken(key,token) {
  return Cookies.set(key, token)
}

export function removeToken(key) {
  return Cookies.remove(key)
}