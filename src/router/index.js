import Vue from 'vue'
import Router from 'vue-router'
import myslef from '@/views/user/myslef'
import homePage from '@/views/home/homePage'
import login from '@/views/user/login'
import otherUser from '@/views/user/otherUser'
import blog from '@/views/blog/blog'
import apprentAgree from '@/views/apprent/apprentAgree'
import setting from '@/views/setting/setting'
import plugin from '@/views/plugin/plugin'


Vue.use(Router)

export default new Router({
    routes: [
        {
            path: '/',
            name: 'login',
            component: login
        },
        {
            path: "/login",
            redirect: "/"
        },
        {
            path: '/register',
            name: 'register',
            component: () => import('@/views/user/register'),
        },
        {
            path: '/myslef',
            name: 'myslef',
            component: myslef
        },
        {
            path: '/homePage',
            name: 'homePage',
            component: homePage
        },
        {
            path: '/otherUser',
            name: 'otherUser',
            component: otherUser
        },
        {
            path: '/blog',
            name: 'blog',
            component: blog
        },
        {
            path: '/apprentAgree',
            name: 'apprentAgree',
            component: apprentAgree
        },
        {
            path: '/editBlog',
            name: 'editBlog',
            component: () => import('@/views/blog/editBlog'),
        },
        {
            path: '/showBlog',
            name: 'showBlog',
            component: () => import('@/views/blog/showBlog'),
        },
        {
            path: '/setting',
            name: 'setting',
            component: setting,
            children: [
                {
                    name: 'userDetail',
                    path: '/setting/userDetail', //表单详情
                    component: () => import('@/views/setting/userDetail'),
                },
                {
                    name: 'message',
                    path: '/setting/message', //表单详情
                    component: () => import('@/views/setting/message'),
                },
                {
                    name: 'apprentice',
                    path: '/setting/apprentice', //表单详情
                    component: () => import('@/views/setting/apprentice'),
                },
                {
                    name: 'master',
                    path: '/setting/master', //表单详情
                    component: () => import('@/views/setting/master'),
                }
            ]
        },
        {
            path: '/plugin',
            name: 'plugin',
            component: plugin,
            children: [
                {
                    name: 'jsonCode',
                    path: '/plugin/jsonCode',
                    component: () => import('@/views/plugin/jsonCode'),
                }

            ]
        },
        {
            path: '/chat',
            name: 'chat',
            component: () => import('@/views/chat/chat')

        },
        {
            path: '/notify',
            name: 'notify',
            component: () => import('@/views/setting/notify')

        },
        {
            path: '/dynamic',
            name: 'dynamic',
            component: () => import('@/views/dynamic/dynamic')

        },
        {
            path: '/dynamicDetail',
            name: 'dynamicDetail',
            component: () => import('@/views/dynamic/dynamicDetail')

        },
        {
            path: '/search',
            name: 'search',
            component: () => import('@/views/search/search')

        }
    ]
})
