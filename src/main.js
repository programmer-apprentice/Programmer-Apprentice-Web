import Vue from 'vue'
import App from './App.vue'
import router from './router'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'

import mavonEditor from 'mavon-editor'
import 'mavon-editor/dist/css/index.css'

import  '@/assets/icon/iconfont.css'

// use
Vue.use(mavonEditor)

Vue.use(ElementUI)
Vue.config.productionTip = false
document.title = "程序员师徒系统"

new Vue({
  render: h => h(App),
  router
}).$mount('#app')
