module.exports = {
  devServer: {
    host: '0.0.0.0',
    port: '8080',
    proxy: {
      '/api': {
        // target: 'http://localhost:8088/api', //需要请求的目标接口
        target: 'http://120.76.201.118:8088/api', //需要请求的目标接口
        changeOrigin: true, // 如果接口跨域，需要进行这个参数配置
        pathRewrite: {
          '/api': ''//这里理解成用‘/api’代替target里面的地址
        }
      },
    }
   
  },
  lintOnSave: false //防止出现编译过程中的阻断启动
}